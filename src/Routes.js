import Vue from 'vue';
import Router from 'vue-router';

import Layout from '@/components/Layout/Layout';
import Login from '@/pages/Login/Login';
import ErrorPage from '@/pages/Error/Error';
import SupportPage from '@/pages/Error/Support';

// Home
import Home from '@/pages/Home/Basic';

// Stock
import Stock from '@/pages/Stock/Basic';

// Employe
import Employe from '@/pages/Employe/Basic';

// Affectation
import Affectation from '@/pages/Affectation/Basic';

// Main
import AnalyticsPage from '@/pages/Dashboard/Dashboard';

// Search view
import Search from '@/pages/Globalsearch/SearchPage';
import Searchresult from '@/pages/Globalsearch/Searchresult';

// Charts
//import ChartsPage from '@/pages/Charts/Charts';

// Ui
import IconsPage from '@/pages/Icons/Icons';
// import NotificationsPage from '@/pages/Notifications/Notifications';


Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/error',
      name: 'Error',
      component: ErrorPage,
    },
    {
      path: '/app',
      name: 'Layout',
      component: Layout,
      children: [
        {
          path: 'support',
          name: 'Support',
          component: SupportPage,
        },
        {
          path: 'dashboard',
          name: 'AnalyticsPage',
          component: AnalyticsPage,
        },
        {
          path: 'components/icons',
          name: 'IconsPage',
          component: IconsPage,
        },
        // {
        //   path: 'notifications',
        //   name: 'NotificationsPage',
        //   component: NotificationsPage,
        // },

        {
          path: 'stock',
          name: 'Stock',
          component: Stock,
        },
        {
          path: 'home',
          name: 'Home',
          component: Home,
        },
        {
          path: 'employe',
          name: 'Employe',
          component: Employe,
        },
        {
          path: 'affectation',
          name: 'Affectation',
          component: Affectation,
        },
        {
          path: 'search/:searchvalue',
          name: 'Searche',
          component: Search,
        },
        {
          path: 'searchdetail/:entity/:id',
          name: 'Searche',
          component: Searchresult,
        },
      ],
    },
  ],
});
