import model from "./model";

var pdfMake = require("pdfmake/build/pdfmake.min.js");
var pdfFonts = require("pdfmake/build/vfs_fonts.js");
pdfMake.vfs = pdfFonts.pdfMake.vfs;

export default {

    listpdf: function (affectationlist) {

        //$("#pdfloader").show();
        //var cartpackagings = [];
        var tr = [
            {text: "#", bold: true,},
            //{text: "Groupe", bold: true, },
            {text: "date de dotation", bold: true,},
            {text: "Employee", bold: true,},
            {text: "Equipment", bold: true,},
            //{text: "Stock ", bold: true, },
            //{text: "Bilan", bold: true, },
        ];
        var body = [];
        body.push(tr);

        $.each(affectationlist, function (i, affectation) {

            var equipmentnames = [];

            affectation.affequipments.forEach((equipment) => {
                equipmentnames.push(`${equipment.label} [${equipment.quantity}] `);
            });

            var tr = [
                affectation.id,
                //affectation.group,
                affectation.affected_at,
                affectation.employee.name,
                {text: equipmentnames.join("\n "),},
                // {text: affectation.duration, style: ["number"]},
            ];
            body.push(tr);

        });

        console.log(body);

        var docDefinition = {
            //pageOrientation: 'landscape',
            pageMargins: [30, 30, 30, 20],
            styles: {
                header: {
                    fontSize: 12,
                    margin: [0, 0, 0, 0],
                    // bold: true,
                    //alignment: 'center'
                },
                title: {

                    //margin: [3, 6],
                    fontSize: 14,
                    alignment: 'center'
                },
                title2: {},
                small: {
                    fontSize: 9,
                },
                center: {
                    alignment: 'center'
                },
                number: {
                    alignment: 'right'
                },
                desc: {
                    margin: [0, 0],
                    fontSize: 10,
                }
            },
//            background: {
//                image: $("#base64").text(),
//            },
            defaultStyle: {
                font: 'GOTHIC'
            },
            content: [
                {
                    columns: [
                        {image: model.getLocalSession().logobase64, width: 100},
                        {
                            width: '50%',
                            text: [
                                {text: "Protector", fontSize: 14, bold: true},
                                {text: "\n\nGestionnaire d'EPI", style: ['small']}
                            ],
                            margin: [25, 0]
                        },
                        {
                            width: '30%',
                            text: [
                                {text: "Liste des affectations", fontSize: 10,},
                            ],
                            //margin: [35,45]
                        },

                    ],
                    style: ["header"], margin: [0, 3]
                },
                {
                    table: {
                        headerRows: 1,
                        widths: ["auto", "auto", "auto", "*"],

                        body: body
                    },
                    //fontSize: 10,
                    margin: [0, 2]
                }

            ]
        };
        pdfMake.fonts = {
            // Default font should still be available
            Agency: {
                normal: 'Agency.ttf',
                bold: 'Agency.ttf',
                italics: 'Agency.ttf',
                bolditalics: 'Agency.ttf'
            },
            GOTHIC: {
                normal: 'GOTHIC.ttf',
                bold: 'GOTHICB.ttf',
                italics: 'GOTHICI.ttf',
                bolditalics: 'GOTHIC.ttf'
            }
        };

//                console.log("print pdf");
        // open the PDF in a new window
        pdfMake.createPdf(docDefinition).open();

        // pdfMake.createPdf(docDefinition).getDataUrl(function(dataURL) {
        //     data = dataURL;
        //     //console.log(data);
        //     $("#affectationmodal").modal("show");
        //     $("#affectationmodal").find(".modal-body").html(`<embed width="1000" height="1000" src="${dataURL}">`);
        // //
        //     //var oburl = window.URL.createObjectURL(dataURL);
        //     // window.open("blob:http://127.0.0.1/162ed054-2182-4861-9cff-e5954a1e5c28",
        //     //     "Product list","menubar=no, status=no, scrollbars=no, menubar=no, width=800, height=900");
        // });

        //// download the PDF
        //pdfMake.createPdf(docDefinition).download('expor_affectation.pdf');

        //return pdfMake.createPdf(docDefinition);


// // print the PDF
// pdfMake.createPdf(docDefinition).print();
//
// // download the PDF
// pdfMake.createPdf(docDefinition).download('optionalName.pdf');
    },

    dotationpdf: function (affectation, affectationkits, affectationequipments, image) {

        //$("#pdfloader").show();
        //var cartpackagings = [];
        var tr = [
            //{text: "#", style: ['thead']},
            {text: "Designation de l'Equipement", style: ['thead']},
            {text: "Quantité", style: ['thead']},
            {
                text: [
                    {
                        text: "Fréquence de renouvellement (en Mois)\n ",
                    },
                    {
                        text: "(sous réserve d’une usure prématurée ou d’une dégradation exceptionnelle)", bold: false, fontSize: 9,

                    },
                ], style: ['thead']
                // text: "Fréquence de renouvellement (en Mois) (sous réserve d’une usure " +
                //     "prématurée ou d’une dégradation exceptionnelle)",

            },
        ];
        var body = [];
        body.push(tr);

        affectationequipments.forEach(function (affectationequipment) {

            var itemname = [];
            affectationequipment.equipmentitems.forEach((item) => {
                itemname.push(item.equipmentitem.code)
            });

            var tr = [
                //affectationequipment.id,
                //affectation.group,
                affectationequipment.label + "\n________________________\nCode : " + itemname.join(", "),
                {text: affectationequipment.quantity, style: ["number"]},
                {text: affectation.duration, style: ["number"]},
                //{text: affectation.stock.quantityprint, style: ["number"] },
                //{text: affectation.stock.physicprint, style: ["number"] },
            ];
            body.push(tr);

        });

        console.log(body);

        var docDefinition = {
            //pageOrientation: 'landscape',
            pageMargins: [50, 50, 50, 50],

            footer: {
                columns: [
                    '',
                    {text: 'www.protector.com', alignment: 'right', margin: [0, 10, 50, 0]}
                ]
            },

            styles: {
                header: {
                    fontSize: 11,
                    margin: [0, 0, 0, 0]
                },
                title: {
                    fontSize: 14,
                    alignment: 'center'
                },
                thead: {
                    bold: true,
                    margin: [8, 8],
                    fontSize: 11,
                    alignment: 'center'
                },
                small: {
                    fontSize: 9,
                },
                center: {
                    alignment: 'center'
                },
                number: {
                    alignment: 'right'
                },
                desc: {
                    margin: [0, 0],
                    fontSize: 10,
                },
                p: {
                    margin: [0, 10],
                },
                psmall: {
                    margin: [0, 5],
                }
            },
//            background: {
//                image: $("#base64").text(),
//            },
            defaultStyle: {
                font: 'GOTHIC'
            },
            content: [
                {
                    columns: [
                        {image: model.getLocalSession().logobase64, width: 100},
                        {
                            width: '50%',
                            text: [
                                {
                                    text: "FICHE DE DOTATION \n\n", bold: true, fontSize: 12,
                                    color: [0, 89, 155]
                                },
                                {
                                    text: "EQUIPEMENTS DE PROTECTION" +
                                        " \nINDIVIDUELLE", bold: true, fontSize: 12,
                                    color: [0, 89, 155]
                                },
                            ],
                            margin: [25, 0]
                        },
                        {
                            width: '30%',
                            text: [
                                {text: `Service Prévention et Sécurité au Travail`, fontSize: 9,},
                                {text: `Fiche Santé et Travail n° ${affectation.id}`, fontSize: 9,},
                                {text: `Date : ${affectation.currentday}_VF`, fontSize: 9,},
                            ],
                            //margin: [35,45]
                        },

                    ],
                    style: ["header", "p"]
                },
                {
                    image: image,
                    width: 450
                },
                {
                    columns: [
                        [
                            {text: "SERVICE : " + affectation.employee.site.name, bold: true, style: ["psmall"]},
                            {text: "NOM :      " + affectation.employee.name, style: ["psmall"]},
                            {text: "DATE :      " + affectation.creationdate, style: ["psmall"]},
                        ],
                        [
                            {text: "METIER :    _____________________________", bold: true, style: ["psmall"]},
                            {text: "PRENOM :  " + affectation.employee.lastname, style: ["psmall"]},
                        ],

                    ],
                    style: ["header", "p"], margin: [0, 3],
                    fontSize: 11,
                },
                {
                    table: {
                        headerRows: 1,
                        widths: ["*", 80, 180],

                        body: body
                    },
                    fontSize: 10,
                    margin: [0, 2]
                },
                {
                    text: "Cette dotation en Equipements de Protection Individuelle (EPI) vous est fournie personnellement, " +
                        "par l’Autorité Territoriale, sur la base de l’identification des risques auxquels vous êtes exposés.",
                    style: ['p'],
                    fontSize: 10,
                },
                {
                    text: "Conformément à l’article L 4122-1 du code du travail, il vous appartient de porter ces équipements " +
                        "et de signaler toute défectuosité ou usure prématurée afin de les remplacer.", style: ['p'],
                    fontSize: 10,
                },
                {
                    text: "Au-delà des risques pour votre intégrité physique, le non-respect de ces consignes vous expose " +
                        "à des sanctions disciplinaires.", style: ['p'],
                    fontSize: 10,
                },
                {
                    table: {
                        headerRows: 1,
                        widths: ["auto", "auto"],

                        body: [
                            [
                                {
                                    columns: [
                                        [
                                            {text: `Je soussigné : ${affectation.employee.name} .`, style: ["psmall"]},
                                            {
                                                text: "reconnais avoir reçu ma dotation ainsi que les informations " +
                                                    "relatives à l’utilisation, l’entretien et à l’obligation de port de ces " +
                                                    "équipements.", style: ["psmall"]
                                            },
                                        ]
                                    ]
                                },
                                {
                                    columns: [
                                        [
                                            {text: `Fait à …………………………………………………`, style: ["psmall"]},
                                            {text: "Le………………………………………………………", style: ["psmall"]},
                                            {text: "Signature \n\n\n\n", style: ["psmall"]},
                                        ]
                                    ]
                                }
                            ]
                        ]
                    },
                    fontSize: 10,
                    margin: [0, 2]
                },

            ]
        };
        pdfMake.fonts = {
            // Default font should still be available
            Agency: {
                normal: 'Agency.ttf',
                bold: 'Agency.ttf',
                italics: 'Agency.ttf',
                bolditalics: 'Agency.ttf'
            },
            GOTHIC: {
                normal: 'GOTHIC.ttf',
                bold: 'GOTHICB.ttf',
                italics: 'GOTHICI.ttf',
                bolditalics: 'GOTHIC.ttf'
            }
        };
//                console.log("print pdf");
        // open the PDF in a new window
        pdfMake.createPdf(docDefinition).open();

        // pdfMake.createPdf(docDefinition).getDataUrl(function(dataURL) {
        //     data = dataURL;
        //     //console.log(data);
        //     $("#affectationmodal").modal("show");
        //     $("#affectationmodal").find(".modal-body").html(`<embed width="1000" height="1000" src="${dataURL}">`);
        // //
        //     //var oburl = window.URL.createObjectURL(dataURL);
        //     // window.open("blob:http://127.0.0.1/162ed054-2182-4861-9cff-e5954a1e5c28",
        //     //     "Product list","menubar=no, status=no, scrollbars=no, menubar=no, width=800, height=900");
        // });

        //// download the PDF
        //pdfMake.createPdf(docDefinition).download('fiche_dotation.pdf');
        //return pdfMake.createPdf(docDefinition);


// // print the PDF
// pdfMake.createPdf(docDefinition).print();
//
// // download the PDF
// pdfMake.createPdf(docDefinition).download('optionalName.pdf');
    },

}