var pdfMake = require("pdfmake/build/pdfmake.min.js");
var pdfFonts = require("pdfmake/build/vfs_fonts.js");
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import model from './model';

export default {

    listpdf: function (affectationlist) {

    },

    lifecycle: function (equipmentstock, equipmentitems) {

        //$("#pdfloader").show();
        //var cartpackagings = [];
        var tr = [
            //{text: "#", style: ['thead']},
            {text: "Code de l'Equipement", style: ['thead']},
            {text: "Date Achate", style: ['thead']},
            {text: "Date de Première utilisation", style: ['thead']},
            {text: "Date de Mise en Rebut", style: ['thead']},
            {text: "Nbre D'utilisation", style: ['thead']},
            {text: "Fournisseur", style: ['thead']},
        ];
        var body = [];
        body.push(tr);

        equipmentitems.forEach(function (equipmentitem) {

            var itemname = [];
            // equipmentitem.affectations.forEach((item)=>{
            //     itemname.push(item.equipmentitem.code)
            // });

            var tr = [
                //affectationequipment.id,
                //affectation.group,
                {text: equipmentitem.code, bold: true},
                {text: equipmentitem.purchasedate,},
                {text: equipmentitem.firstaffectation.creationdate ? equipmentitem.firstaffectation.creationdate : '',},
                {text: equipmentitem.rebutdate ? equipmentitem.rebutdate : '',},
                {text: equipmentitem.nbaffectations, style: ["number"]},
                {text: equipmentitem.provisionequipment.provider.name,},
                //{text: affectation.stock.quantityprint, style: ["number"] },
                //{text: affectation.stock.physicprint, style: ["number"] },
            ];
            body.push(tr);

        });

        console.log(body);

        var docDefinition = {
            //pageOrientation: 'landscape',
            pageMargins: [50, 50, 50, 50],

            footer: {
                columns: [
                    '',
                    {text: 'www.protector.com', alignment: 'right', margin: [0, 10, 50, 0]}
                ]
            },

            styles: {
                header: {
                    fontSize: 11,
                    margin: [0, 0, 0, 0]
                },
                title: {
                    fontSize: 14,
                    alignment: 'center'
                },
                thead: {
                    bold: true,
                    margin: [8, 8],
                    fontSize: 11,
                    alignment: 'center'
                },
                small: {
                    fontSize: 9,
                },
                center: {
                    alignment: 'center'
                },
                number: {
                    alignment: 'right'
                },
                desc: {
                    margin: [0, 0],
                    fontSize: 10,
                },
                p: {
                    margin: [0, 10],
                },
                psmall: {
                    margin: [0, 5],
                }
            },
            // background: {
            //     image: model.getLocalSession().logobase64,
            // },
            defaultStyle: {
                font: 'GOTHIC'
            },
            content: [
                {
                    columns: [
                        {image: model.getLocalSession().logobase64, width: 100},
                        {
                            width: '50%',
                            text: [
                                {
                                    text: "FICHE DE VIE \n\n", bold: true, fontSize: 12,
                                    color: [0, 89, 155]
                                },
                                {
                                    text: "EQUIPEMENTS DE PROTECTION" +
                                        " \nINDIVIDUELLE", bold: true, fontSize: 12,
                                    color: [0, 89, 155]
                                },
                            ],
                            margin: [25,0]
                        },
                        {
                            width: '30%',
                            text: [
                                {text: `Service Prévention et Sécurité au Travail`, fontSize: 9,},
                                {text: `Fiche Santé et Travail n° 121`, fontSize: 9,},
                                {text: `Date : 25/11/2016_VF`, fontSize: 9,},
                            ],
                            //margin: [35,45]
                        },

                    ],
                    style: ["header", "p"]
                },
                {
                    columns: [
                        [
                            {text: "\n\nEquipment :      " + equipmentstock.equipment.name, style: ["psmall"]},
                        ],

                    ],
                    style: ["header", "p"], margin: [0, 3],
                    fontSize: 11,
                },
                {
                    table: {
                        headerRows: 1,
                        widths: ["*", 80, 80, 80, 50, "auto"],

                        body: body
                    },
                    fontSize: 10,
                    margin: [0, 2]
                }
            ]
        };
        pdfMake.fonts = {
            // Default font should still be available
            Agency: {
                normal: 'Agency.ttf',
                bold: 'Agency.ttf',
                italics: 'Agency.ttf',
                bolditalics: 'Agency.ttf'
            },
            GOTHIC: {
                normal: 'GOTHIC.ttf',
                bold: 'GOTHICB.ttf',
                italics: 'GOTHICI.ttf',
                bolditalics: 'GOTHIC.ttf'
            }
        };

        // open the PDF in a new window
        pdfMake.createPdf(docDefinition).open();

    }

}