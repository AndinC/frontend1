/**
 * Created by Aurelien Atemkeng on 7/26/2018.
 */
import $ from 'jquery';

export default {
    baseredirect : "index.php",
    baseurl : "http://127.0.0.1/protector/",
    //baseurl : "http://protector.spacekola.com/",
    //baseurl : "http://192.168.100.15:8080/",
    lang: {},
    formatDate(date) {
        // var monthNames = [
        //     "01", "01", "01",
        //     "01", "01", "01", "01",
        //     "01", "01", "10",
        //     "11", "12"
        // ];
        // var monthNames = [
        //     "January", "February", "March",
        //     "April", "May", "June", "July",
        //     "August", "September", "October",
        //     "November", "December"
        // ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
        // monthNames[monthIndex]
        return  year + '-' + monthIndex+1 + '-' + day;
    },
    initlang(lang = "en"){
        $.getJSON( this.baseurl+"web/assets/lang/"+lang+".json", ( data ) => {
            console.log(data);
            localStorage.setItem('protector_lang', JSON.stringify(data))
            this.lang = data;
        });
    },
    getLocalSession(key = ""){
        return JSON.parse(localStorage.getItem('protector_session'))
    },

    url: function (route, parameter) {
        var getAttr = "";
        if (parameter) {
            var keys = Object.keys(parameter);
            var values = Object.values(parameter);
            for (var i = 0; i < keys.length; i++) {
                getAttr += "&" + keys[i] + "=" + values[i];
            }
        }

        return route + getAttr;

    },
    routing: function (route, parameter) {
        return this.baseurl + "?path=" + this.url(route, parameter);
    },
    entity : null,
    _formdata : function (form, formdata) {
        var $inputs = form.find('input');
        var $textareas = form.find('textarea');
        var $selects = form.find('select');
        var formentity = {};

        if (!formdata)
            formdata = new FormData();

        $.each($inputs, function (i, input) {

            if($(input).attr('type') === "file" && input.files[0]){
                formdata.append($(input).attr('name'), input.files[0]);
                formentity[$(input).attr('name')] = $(input).val();
            }
            else if($(input).attr('type') === "checkbox" && input.checked){
                formdata.append($(input).attr('name'), $(input).val());
                formentity[$(input).attr('name')] = $(input).val();
            }
            else if($(input).attr('type') === "radio" && input.checked){
                formdata.append($(input).attr('name'), $(input).val());
                formentity[$(input).attr('name')] = $(input).val();
            }
            else if($(input).attr('type') === "password" ) {
                formdata.append($(input).attr('name'), $(input).val());
                formentity[$(input).attr('name')] = $(input).val();
            }
            else if($(input).attr('type') === "email" ) {
                formdata.append($(input).attr('name'), $(input).val());
                formentity[$(input).attr('name')] = $(input).val();
            }
            else if($(input).attr('type') === "number" ) {
                formdata.append($(input).attr('name'), $(input).val());
                formentity[$(input).attr('name')] = $(input).val();
            }
            else if($(input).attr('type') === "text" ) {
                formdata.append($(input).attr('name'), $(input).val());
                formentity[$(input).attr('name')] = $(input).val();
            }
        });
        $.each($textareas, function (i, textarea) {
            formdata.append($(textarea).attr('name'), $(textarea).val());
            formentity[$(textarea).attr('name')] = $(textarea).val();
        });
        $.each($selects, function (i, select) {
            formdata.append($(select).attr('name'), $(select).val());
            formentity[$(select).attr('name')] = {
                value: $(select).val(),
                option: $(select).find(":selected").text(),
            }
        });

        model.formentity = formentity;
        return formdata;
    },

    _apiget : function (action, data, callback) {

        //console.log(typeof data)
        if(!callback){
            callback = data;
            data = {};
        }

        $.ajax({
            url: this.baseurl+'api/'+action,
            data: data,
            method: "GET",
            dataType: "json",
            success: callback,
            error: function (e) {
                console.log(e);//responseText

            }
        });
    },

    _apipost : function (action, formdata, callback, fd = true) {

        if(!fd){

            $.ajax({
                url: this.baseurl+'api/'+action,
                data: formdata,
                method: "POST",
                dataType: "json",
                success: callback,
                error: function (e) {
                    console.log(e);//responseText
                    callback(e)
                }
            });

            return;
        }

        $.ajax({
            url: this.baseurl+'api/'+action,
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            method: "POST",
            dataType: "json",
            success: callback,
            error: function (e) {
                console.log(e);//responseText
                callback(e.responseText);
            }
        });
    },

    getformvalue: function (field) {
        return this.formentity[this.entity+"_form["+field+"]"];
    },
    getform: function (fm, entity, attribs) {
        this._formdata(fm);
        var keys = Object.keys(this.formentity);
        var values = Object.values(this.formentity);
        var form = [];
        var fd = new FormData();
        attribs.forEach((attr)=>{
            for (var i = 0; i < keys.length; i++) {

                if(keys[i] === entity+"_form["+attr+"]"){
                    form[attr] = values[i];
                    console.log(typeof values[i]);
                    if(typeof values[i] === 'string')
                        fd.append(keys[i], values[i]);
                    else
                        fd.append(keys[i], values[i].value);

                    break;
                }

            }
        });

        //form['dvups_form['+entity+']'] = this.formentity['dvups_form['+entity+']'];
        form[entity] = this.formentity['dvups_form['+entity+']'];
        console.log(JSON.parse(form[entity]));
        fd.append('dvups_form['+entity+']', this.formentity['dvups_form['+entity+']']);
        form.fd = fd;
        return form;
    },

    entitytoformdata (entity, entityname){
        var fd = new FormData();
        this.formentity = {};
        var keys = Object.keys(entity);
        var values = Object.values(entity);

        for (var i = 0; i < keys.length; i++) {
            if (typeof values[i] === 'object' && values[i] !== null)
                fd.append(entityname+`.${keys[i]}>id`, values[i].id)
            else
                fd.append(entityname+`.${keys[i]}`, values[i])
        }

        return fd;
    },

    entitytoformentity (entity, objectdetection = ".id"){

        var formentity = {};
        var keys = Object.keys(entity);
        var values = Object.values(entity);

        for (var i = 0; i < keys.length; i++) {
            if (typeof values[i] === 'object' && values[i] !== null){
                console.log(values[i]);
                formentity[keys[i]+objectdetection] = values[i].id;
            }
            else
                formentity[keys[i]] = values[i]
        }

        return formentity;
    },
    getformfield: function (field) {
        return $("input[name='"+this.entity+"_form["+field+"]']");
    }
};
