// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'expose-loader?jQuery!jquery' // eslint-disable-line
import 'expose-loader?$!jquery' // eslint-disable-line
import Vue from 'vue';
import VueI18n from 'vue-i18n';

import BootstrapVue from 'bootstrap-vue';
import VueTouch from 'vue-touch';
import Trend from 'vuetrend';
import vmodal from 'vue-js-modal'

import store from './store';
import router from './Routes';
import App from './App';
import lang from './devupsjs/lang';

Vue.use(VueI18n)
Vue.use(vmodal);
Vue.use(BootstrapVue);
Vue.use(VueTouch);
Vue.use(Trend);

Vue.config.productionTip = false;
// Ready translated locale messages
const messages = lang;
//     {
//   en: {
//     message: {
//       hello: 'hello world'
//     }
//   },
//   ja: {
//     message: {
//       hello: 'こんにちは、世界'
//     }
//   }
// }

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'fr', // set locale
  messages, // set locale messages
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  store,
  router,
  render: h => h(App),
});
